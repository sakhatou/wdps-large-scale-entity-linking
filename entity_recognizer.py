from html2text import HTML2Text
import spacy
import requests
import collections
import string
from map_entity_types import map_entity_type
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

es_url = ""
trident_url = ""


def html_to_text(document):
	warc_trec_id, html = document

	h = HTML2Text()
	h.ignore_links = True
	h.images_to_alt = True
	h.ignore_emphasis = True

	yield warc_trec_id, h.handle(html)


nlp = spacy.load("en")


def text_to_entities(document):
	warc_trec_id, text = document

	entities = set()

	for entity in nlp(text).ents:
		if entity.label_ not in ["CARDINAL", "ORDINAL", "QUANTITY", "MONEY", "PERCENT", "TIME"]:
			entity_str = "".join(x for x in entity.text if x in string.printable)
			entity_str = entity_str.replace("\n", " ").strip()
			if len(entity_str) >= 2:
				entities.add((entity_str, entity.label_))
	if len(entities) > 0:
		yield warc_trec_id, entities


def search_es(query, session):
	entity_str, label = query
	try:
		response = session.get(es_url, params={'q': entity_str, 'size': 1000})
		results = collections.defaultdict(float)
		if response:
			response = response.json()
			for hit in response.get("hits", {}).get("hits", []):
				fb_label = hit.get("_source", {}).get("label")
				fb_id = hit.get("_source", {}).get("resource")
				es_score = hit.get("_score")
				results[(fb_id, fb_label.lower())] += es_score

		result_list = []
		for key, val in results.items():
			result_list.append((key[0], key[1], val))
		result_list.sort(key=lambda x: x[2], reverse=True)
		if len(result_list) > 0:
			return (entity_str, label, result_list[:10])
	except requests.exceptions.RequestException as e:
		print(e)

def search_entities(entities):
	warc_trec_id, entity_set = entities
	# entity_set: {(entity_str, label)}

	session = requests.session()
	retries = Retry(total=5, backoff_factor=1)
	session.mount('http://', HTTPAdapter(max_retries=retries))

	result = []
	for entity in entity_set:
		lookup = search_es(entity, session)
		if lookup is not None:
			result.append(lookup)

	#result = list(map(lambda e: search_es(e, session), entity_set))
	#print(result)
	if len(result) > 0:
		yield warc_trec_id, result


def search_trident(query, session):
	fb_id, ontology = query

	query_str = """select distinct * where {{
	?s <http://www.w3.org/2002/07/owl#sameAs> <http://rdf.freebase.com/ns/m.{}> .
	?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> {} .
}}"""

	query_str = query_str.format(fb_id[3:], ontology)
	try:
		response = session.post(trident_url, data={"print": True, "query": query_str})
		#print("trident query")
		if response:
			response = response.json()
			stats = response.get("stats", {})
			if(stats != {}):
				return int(stats.get("nresults"))
		else:
			print("Error: could not get results from trident!\n{}".format(response.text))
	except requests.exceptions.RequestException as e:
		print(e)
	except json.decoder.JSONDecodeError as e:
		print("> Error decoding json")
	return 0


def link_entity(warc_trec_id, entity_str, es_results, label, session):
	#print("> link entity - {}".format(entity_str))
	for es_result in es_results:
		fb_id, fb_label, es_score = es_result
		potential_ontologies = map_entity_type(label)
		for ontology in potential_ontologies:
			result_count = search_trident((fb_id, ontology), session)
			#print("{}, {}, {}".format(entity_str, label, result_count))
			if result_count > 0:
				return warc_trec_id, entity_str, fb_id


def link_entities(document):
	warc_trec_id, resolved_entities = document
	session = requests.session()
	retries = Retry(total=5, backoff_factor=1)
	session.mount('http://', HTTPAdapter(max_retries=retries))
	for entity in resolved_entities:
		entity_str, label, es_results = entity
		link = link_entity(warc_trec_id, entity_str, es_results, label, session)
		if link is not None:
			yield link
