#!/usr/bin/env bash

SCRIPT="er_mp.py"
INFILE=${1:-"sample-recomp.warc.gz"}
OUTFILE=${2:-"output.tsv"}

ES_BIN=${3:-$(realpath ~/scratch/elasticsearch-2.4.1/bin/elasticsearch)}
ID=${4:-"0"}

if [[ ! -d logs ]]; then
  mkdir logs
fi

# Start elasticsearch
ES_PORT=9200

#>logs/es.log*
prun -o logs/es.log.${ID} -v -np 1 ESPORT=${ES_PORT} ${ES_BIN} </dev/null 2> logs/es.node.${ID} &
echo "($ID) waiting for elasticsearch to set up..."
ES_NODE=""
until [ -n "${ES_NODE}" ]; do ES_NODE=$(cat logs/es.node.${ID} | grep '^:' | grep -oP '(node...)'); done
ES_PID=$!
until [[ -n "$(cat logs/es.log.${ID}* | grep YELLOW)" ]]; do sleep 1; done
echo "($ID) elasticsearch should be running now on node $ES_NODE:$ES_PORT (connected to process $ES_PID)"

# Start trident
#KB_PORT=9090
#KB_BIN=/home/bbkruit/scratch/trident/build/trident
#KB_PATH=/home/jurbani/data/motherkb-trident

#prun -o logs/kb.log -v -np 1 $KB_BIN server -i $KB_PATH --port $KB_PORT </dev/null 2> logs/kb.node &

#echo "waiting 5 seconds for trident to set up..."
#sleep 5

#KB_NODE=$(cat logs/kb.node | grep '^:' | grep -oP '(node...)')
#KB_PID=$!
#echo "trident should be running now on node $KB_NODE:$KB_PORT (connected to process $KB_PID)"

echo "($ID) Running entity recognition script at $(date)"

prun -o ${OUTFILE} -v -np 1 python3.5 ${SCRIPT} ${INFILE} ${OUTFILE} ${ES_NODE}:${ES_PORT} ${KB_NODE}:$KB_PORT

echo "($ID) Finished at $(date)"

mv ${OUTFILE}.0 ${OUTFILE}

kill ${ES_PID}
echo "($ID) Stopped ES (pid ${ES_PID})"
#kill ${KB_PID}
#echo "Killed trident (pid ${KB_PID})"
