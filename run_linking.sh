#!/usr/bin/env bash

SCRIPT="er_linking.py"
INFILE=${1:-$(readlink -f "after_es.pkl")}
OUTFILE=${2:-"sample_after_linking.tsv"}

# Start trident
KB_PORT=9090
KB_BIN=/home/bbkruit/scratch/trident/build/trident
KB_PATH=/home/jurbani/data/motherkb-trident

prun -o logs/kb.log -v -np 1 $KB_BIN server -i $KB_PATH --port $KB_PORT </dev/null 2> logs/kb.node &

until [ -n "$KB_NODE" ]; do KB_NODE=$(cat logs/kb.node | grep '^:' | grep -oP '(node...)'); done
echo "waiting 5 seconds for trident to set up..."
sleep 10

KB_PID=$!
echo "trident should be running now on node $KB_NODE:$KB_PORT (connected to process $KB_PID)"

echo "Running entity linking script at $(date)"

prun -o ${OUTFILE} -v -np 1 python3.5 -u ${SCRIPT} ${INFILE} ${OUTFILE} ${KB_NODE}:$KB_PORT

echo "Finished at $(date)"

kill ${KB_PID}
echo "Killed trident (pid ${KB_PID})"
