import pickle
import sys

in_file = sys.argv[1]

def records():
        with open(in_file, "rb") as input:
                while True:
                        try:
                                yield pickle.load(input)
                        except EOFError:
                                break


for record in records():
	if record:
		warc_trec_id, entities = record
		if entities:
			for entity in entities:
				entity_str, ner_label, es_results = entity
				#if es_results:
				if es_results and es_results[0][2] > 40:
					#print("{}\t{}\t{}\t{}\t{}".format(warc_trec_id, entity_str, es_results[0][0], ner_label, es_results[0][2]))
					print("{}\t{}\t{}".format(warc_trec_id, entity_str, es_results[0][0]))
