import sys
from multiprocessing import pool
import requests
import entity_recognizer
import pickle

in_filename = sys.argv[1]
out_filename = sys.argv[2]
trident_domain = sys.argv[3]
entity_recognizer.trident_url = "http://{}/sparql".format(trident_domain)


def records():
	with open(in_filename, "rb") as input:
		while True:
			try:
				yield pickle.load(input)
			except EOFError:
				break


def work(record):
	#for linked_entity in entity_recognizer.link_entities(record):
	#	print(linked_entity)
	return list(entity_recognizer.link_entities(record))

with pool.Pool(processes=4) as pool:
	with open(out_filename, "w") as out:
		for entity_list in pool.imap(work, records(), 2):
			for entity in entity_list:
				warc_trec_id, entity_str, fb_id = entity
				print("{}\t{}\t{}".format(warc_trec_id, entity_str, fb_id))
#for record in records():
#	result = work(record)
#	for entity in result:
#		warc_trec_id, entity_str, fb_id = entity
#		print("{}\t{}\t{}".format(warc_trec_id, entity_str, fb_id))
