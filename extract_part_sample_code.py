import sys
from multiprocessing import pool
import warcio
import requests
import entity_recognizer
import json
from pprint import pprint

#with open("./part.gz", "rb") as stream:
#    for record in warcio.ArchiveIterator(stream, arc2warc=True):
#        if record.rec_type == 'response':
#            print(record.rec_headers.get_header('WARC-TREC-ID'), "\n".join(record.content_stream().read().decode("utf-8", errors="ignore").splitlines()))


import os
import sys

NUMBER_OF_NODES=int(sys.argv[1])
INPUT_FILE = sys.argv[2]
OUTPUT_DIR = sys.argv[3]
os.system("warcio index " + INPUT_FILE + " -f warc-type,offset,content-length > logs/warc_info.log")
file_content = open("logs/warc_info.log", "r").readlines()
line_count = len(file_content)
previous_offset = 0
for x in range(1, NUMBER_OF_NODES + 1):
        if x >= NUMBER_OF_NODES:
                os.system("dd if=" + INPUT_FILE + " of=" + OUTPUT_DIR + "/part" + str(x) +".warc.gz bs=1 skip=" + str(previous_offset))
        else:
                line_index = min(int(line_count / NUMBER_OF_NODES * x), line_count)
                line_json = json.loads(file_content[line_index])
                offset = line_json["offset"]
                count = int(offset) - previous_offset

                os.system("dd if=" + INPUT_FILE + " of=" + OUTPUT_DIR + "/part" + str(x) + ".warc.gz bs=1 skip=" + str(previous_offset) + " count=" + str(count) + " > " + OUTPUT_DIR + "/part" + str(x) + ".warc.gz")

                previous_offset = int(offset)

