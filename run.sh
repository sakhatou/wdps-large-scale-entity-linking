#!/usr/bin/env bash

SCRIPT="run_mp.sh"
INFILE=${1:-"hdfs:///user/bbkruit/sample.warc.gz"}
OUTFILE=${2:-"output.tsv"}
NR_NODES=${3:-4}
ES_PATH=$(realpath ~/scratch/elasticsearch-2.4.1)

WORK_DIR=$(mktemp -d -t -p ./ er-temp-XXXX)
mkdir "$WORK_DIR/data"
echo "> Created working directory $WORK_DIR"

echo "> Copy warc file from hdfs to work directory"
hdfs dfs -cat ${INFILE} > "$WORK_DIR/data/input.warc.gz"

echo "> Recompress warc file"
warcio recompress ${WORK_DIR}/data/input.warc.gz ${WORK_DIR}/data/input-recomp.warc.gz

echo "> Split warc file into parts"
python extract_part_sample_code.py ${NR_NODES} "$WORK_DIR/data/input-recomp.warc.gz" "$WORK_DIR/data"

echo "> Start processing"
for (( i = 1; i <= ${NR_NODES}; ++i )); do
    echo "[$i] > make private copy of elastic search"
    cp -r ${ES_PATH} "$WORK_DIR/$i/"

    echo "[$i] > run script"
    bash run_mp.sh "${WORK_DIR}/data/part${i}.warc.gz" "${WORK_DIR}/data/output-${i}.tsv" $(readlink -f "$WORK_DIR/$i/bin/elasticsearch") "$i" &
    PIDS[$i]=$!
done

echo "> Waiting for processing to finish"
for pid in ${PIDS[@]}; do
    wait $pid
done

echo "> Concatenating output files"
cat ${WORK_DIR}/data/output-*.tsv > ${OUTFILE}

#echo "> Cleaning up working directory"
#rm -r ${WORK_DIR}

echo "> Done"
echo "> Your output can be found in $OUTFILE"

