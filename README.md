# Web Data Processing Systems
## Assignment 1: Large Scale Entity Linking  - Group 14 - wdps1814

In this assignment we will perform *Entity linking* on a collection of web pages. The input of the bash script is a gzipped WARC file and the output a three-column tab-separated (tsv) file with document IDs, entity surface forms and Freebase entity IDs. 

### The method
The `run.sh` bash script copies the input file from the hdfs to the local filesystem and converts the input file to a warcio readable file.
The warcio library expects each document in the archive to be seperately gzipped, which also conveniently allows us to split up the file more easily.
The third argument of the run.sh script decides the number of parts (4 by default) the input is split into.
For each part an elasticsearch and processing node are set up by `run_mp.sh`. For each elasticsearch node a seperate directory is created to prevent them from conflicting each other. Then in turn the script `er_mp.py` is run. Here, the headers 'WARC-TREC-ID' and HTML content will be extracted from the archive entries. This is forwarded to entity_recognizer.py which tries to find the correct 'Freebase entity IDs'.

## When starting fresh

To execute the bash script and view the results, the repository needs to be imported and a virtual environment needs to be created and set up (in case of not working from our folder). 

### Access to the repository ###
Clone into the working folder the following repository
```bash
git clone https://hasineefe@bitbucket.org/sakhatou/wdps-large-scale-entity-linking.git
```

### Create Python virtual environment 
```bash
python3.5 -m venv ./venv/
source ./venv/bin/activate
```
Whenever pip throws an error of a missing file, download in the working folder ./venv/bin/
https://github.com/pypa/virtualenv/blob/master/virtualenv_embedded/activate_this.py

### Install the necessary packages for recognizing entities + spacy model 
```bash
pip install --ignore-installed html2text spacy warcio
python -m spacy download en
```

### Set the Pythonpath
(might also be needed to download spacy language model)

```bash
PYTHONPATH=~/scratch/wdps-large-scale-entity-linking/venv/:~/scratch/wdps-large-scale-entity-linking/venv/lib/python3.5/site-packages/:~/scratch/wdps-large-scale-entity-linking/venv/lib/python2.7/site-packages/:$PYTHONPATH
```

## When working from our directory

When working from our folder at the DAS-4 (`~/scratch/wdps-large-scale-entity-linking/`), set up the virtual environment by running the following commands
```
source ./venv/bin/activate
PYTHONPATH=~/scratch/wdps-large-scale-entity-linking/venv/:~/scratch/wdps-large-scale-entity-linking/venv/lib/python3.5/site-packages/:~/scratch/wdps-large-scale-entity-linking/venv/lib/python2.7/site-packages/:$PYTHONPATH
```

Hence, the bash script is ready to run with the following command
```bash
bash run.sh [input warc archive] [output tsv file] [nr of parts]
```
The extensions of those optional parameters are respectively as input a hdfs url and as output a local tsv file will be generated. 
The number of parts decides how many parts the input is split up into and with that how many nodes to spawn (2 * nr of parts).
Because all nodes are reserved individually stuff might break if more nodes are requested then are available.