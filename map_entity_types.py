def map_entity_type(type):
    return {
		'PERSON': ['<http://dbpedia.org/ontology/Person>'],
		'NORP': ['<http://dbpedia.org/ontology/EthnicGroup>', 
				'<http://dbpedia.org/ontology/PoliticalParty>'],
		'FAC': ['<http://dbpedia.org/ontology/Infrastructure>', 
				'<http://dbpedia.org/ontology/ArchitecturalStructure>'],
		'ORG': ['<http://dbpedia.org/ontology/Organisation>'],
		'GPE': ['<http://dbpedia.org/ontology/Place>', 
				'<http://dbpedia.org/ontology/Location>'],
		'LOC': ['<http://dbpedia.org/ontology/NaturalPlace>', 
				'<http://dbpedia.org/ontology/MountainRange>', 
				'<http://dbpedia.org/ontology/BodyOfWater>'],
		'PRODUCT': ['<http://www.w3.org/2002/07/owl#Thing>'],
		'EVENT': ['<http://dbpedia.org/ontology/Event>', 
				'<http://dbpedia.org/class/yago/Phenomenon100034213>', 
				'<http://dbpedia.org/class/yago/Event100029378>'],
		'WORK_OF_ART': ['<http://dbpedia.org/ontology/Work>'],
		'LAW': ['<http://dbpedia.org/ontology/Law>'],
		'LANGUAGE': ['<http://dbpedia.org/ontology/Language>'],
		'DATE': ['<http://dbpedia.org/class/yago/TimePeriod115113229>', 
				'<http://dbpedia.org/class/yago/Measure100033615>']
    }[type]