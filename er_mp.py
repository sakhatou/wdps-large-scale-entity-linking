import sys
from multiprocessing import pool
import warcio
import requests
import entity_recognizer
import pickle

in_filename = sys.argv[1]
out_filename = sys.argv[2]
es_domain = sys.argv[3]
entity_recognizer.es_url = "http://{}/freebase/label/_search".format(es_domain)
trident_domain = sys.argv[4]
entity_recognizer.trident_url = "http://{}/sparql".format(trident_domain)


def records():
	with open(in_filename, "rb") as stream:
		for record in warcio.ArchiveIterator(stream, arc2warc=True):
			if record.rec_type == 'response':
				yield record.rec_headers.get_header('WARC-TREC-ID'), \
					"\n".join(record.content_stream().read().decode("utf-8", errors="ignore").splitlines())


def work(record):
	for document in entity_recognizer.html_to_text(record):
		for entity_set in entity_recognizer.text_to_entities(document):
			for resolved_entity in entity_recognizer.search_entities(entity_set):
				return resolved_entity

with pool.Pool(processes=4) as pool:
	with open(out_filename, "wb") as out:
		for record in pool.imap(work, records(), 16):
			if record:
				warc_trec_id, entities = record
				if entities:
					for entity in entities:
						entity_str, ner_label, es_results = entity
						if es_results and es_results[0][2] > 40:
							print("{}\t{}\t{}".format(warc_trec_id, entity_str, es_results[0][0]))

